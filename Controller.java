public class Controller {
    private Student modelS;
    private Teacher modelT;
    private Course modelC;
    private View view;

    public Controller(Student modelS,View view){
        this.modelS = modelS;
        this.view = view;
    }
    public Controller(Teacher modelT,View view){
        this.modelT = modelT;
        this.view = view;
    }
    public Controller(Course modelC , View view){
        this.modelC = modelC;
        this.view = view;
    }

    //student
    public String getStudentName(){
        return modelS.getName();
    }
    public void setStudentName(String name){
        modelS.setName(name);
    }
    public String getStudentLastName(){
        return modelS.getLastName();
    }
    public void setStudentLastName(String lastName){
        modelS.setName(lastName);
    }
    public int getStudentId(){
        return modelS.getId();
    }
    public void setStudentId(int id){
        modelS.setStudentId(id);
    }
    public String getTeacherLastName(){ return modelS.getTeacherName(); }
    public void setTeacherLastName(String lastName){
        modelS.setTeacherName(lastName);
    }


    //teacher
    public String getTeacherName(){
        return modelT.getName();
    }
    public void setTeacherName(String name){
        modelT.setName(name);
    }
    public String getTeacherLastname(){
        return modelT.getLastName();
    }
    public void setTeacherLastname(String lastName){
        modelT.setLastName(lastName);
    }
    public int getTeacherId(){
        return modelT.getId();
    }
    public void setTeacherId(int id){
        modelT.setTeacherId(id);
    }
    public String getStudentLastname(){
        return modelT.getStudentName();
    }
    public void setStudentLastname(String name){
        modelT.setStudentName(name);
    }

    //course
    public String getCourseName(){
        return modelS.getCourseName();
    }
    public void setCourseName(String name){
        modelS.setCourseName(name);
    }
    public int getCourseId(){
        return modelC.getCourseId();
    }
    public void setCourseId(int id){
        modelC.setCourseId(id);
    }

    //view
    public void studentView(){
        view.printStudentDetails(modelS.getId(),modelS.getName(), modelS.getLastName(),modelS.getTeacherName(),modelS.getCourseName());
    }
    public void teacherView(){
        view.printTeacherDetails(modelT.getId(),modelT.getName(),modelT.getLastName(),modelT.getStudentName(),modelT.getCourseName());
    }
    public void courseView(){
        view.printCourseDetails(modelC.getCourseId(),modelC.getName());
    }

}
