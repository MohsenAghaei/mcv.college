//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

public class Teacher extends Person  {
    private int teacherId;
    private Student student;
    private static Teacher Teachers;
    private Course course;

    public Teacher() {
        super();
    }

    protected Teacher(int teacherId, String name, String lastName, int studentId, int courseId) {
        super(name, lastName);
        this.teacherId = teacherId;
    }

    public Teacher(int teacherId, String name, String lastName) {
        super(name, lastName);
        this.teacherId = teacherId;
        this.student.getId();
    }

    public int getTeacherId() {
        return this.teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getStudentName() {
        return this.student.getLastName();
    }

    public void setStudentName(String name) {
        this.student.setLastName(name);
    }

    public String getCourseName() {
        return this.course.getName();
    }

    public void setCourseName(String name) {
        this.course.setName(name);
    }

    public void addTeacher(ArrayList<Teacher> teachers, ArrayList<Student> students, ArrayList<Course> courses, Scanner input) {
        System.out.println("Inter the Teacher ID");
        int teacherId = input.nextInt();
        System.out.println("Inter the name");
        String name = input.next();
        System.out.println("Inter the lastName");
        String lastName = input.next();
        System.out.println("Inter the Student Id");

        int n;
        for(n = 0; n < students.size(); ++n) {
            System.out.println(((Student)students.get(n)).getId() + " " + ((Student)students.get(n)).getName() + " " + ((Student)students.get(n)).getLastName() + "\n");
        }

        n = input.nextInt();

        int courseId;
        for(courseId = 0; courseId < courses.size(); ++courseId) {
            System.out.println(((Course)courses.get(courseId)).getCourseId() + " " + ((Course)courses.get(courseId)).getName() + "\n");
        }

        courseId = input.nextInt();
        Teacher T = new Teacher(teacherId, name, lastName, ((Student)students.get(n)).getId(), ((Course)courses.get(courseId)).getCourseId());
        teachers.add(T);
        System.out.println("Successful");
    }

    public void addStudentsToListTeacher(Map<Integer, Student> LSP, Teacher teacher, ArrayList<Teacher> teachers, ArrayList<Student> students, Scanner input) {
        System.out.print("Which Teacher:\n");
        listOfTeacher(teachers);
        System.out.println("Inter the Code Teacher");
        int CodeT = input.nextInt();
        Teacher T = new Teacher(((Teacher)teachers.get(CodeT)).getId(), ((Teacher)teachers.get(CodeT)).getName(), ((Teacher)teachers.get(CodeT)).getLastName());
        System.out.print("Which Student:\n");
        Student.listOfStudent(students, teacher, this.course);
        System.out.println("Inter the Code Student");
        int CodeS = input.nextInt();
        Student S = new Student(((Student)students.get(CodeS)).getStudentId(), ((Student)students.get(CodeS)).getName(), ((Student)students.get(CodeS)).getLastName(), ((Student)students.get(CodeS)).getScore());
        LSP.put(T.teacherId, S);
        Iterator var10 = LSP.entrySet().iterator();

        while(var10.hasNext()) {
            Entry<Integer, Student> entry = (Entry)var10.next();
            System.out.println("Teacher :" + entry.getKey() + "\nStudents:" + entry.getValue());
        }

        System.out.format("Student. %s with Code. %s added to the list %s \n", S.getLastName(), S.getId(), T.getLastName());
    }

    public static void StudentFromTheListTeacher(Map<Integer, Student> LSP, ArrayList<Teacher> teachers, Scanner input) {
        listOfTeacher(teachers);
        System.out.println("Inter the Teacher ID");
        int studentId = input.nextInt();
        System.out.println(LSP.get(studentId));
    }

    public int getId() {
        return this.teacherId;
    }

    public void setId(int teacherId) {
        this.teacherId = teacherId;
    }


    public static Teacher listOfTeacher(ArrayList<Teacher> teachers) {
        for(int i = 0; i < teachers.size(); ++i) {
            Teacher T = new Teacher(((Teacher)teachers.get(i)).getTeacherId(), ((Teacher)teachers.get(i)).getName(), ((Teacher)teachers.get(i)).getLastName());
            System.out.println("Code :" + i + "\t " + T);
        }

        return Teachers;
    }

    public int getScore() {
        return 0;
    }
}
