public abstract class Person {
    private String name;
    private String lastName;

    protected Person(String name , String lastName){
        this.name = name ;
        this.lastName = lastName;
    }

    public Person() {

    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getLastName(){
        return this.lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    @Override
    public String toString() {
        return  "name : " + name + " " + ", lastName : " + lastName + " :" + ", Id : " + getId() ;
    }
    public abstract int getId();


}
