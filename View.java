//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

public class View {
    public View() {
    }

    public void printStudentDetails(int studentId, String name, String studentLastName, String teacherLastName, String courseName) {
        System.out.println("ID:" + studentId);
        System.out.println("Name:" + name);
        System.out.println("LastName:" + studentLastName);
        System.out.println("Teacher LastName:" + teacherLastName);
        System.out.println("Course Name:" + courseName);
    }

    public void printTeacherDetails(int teacherId, String name, String teacherLastName, String studentLastName, String courseName) {
        System.out.println("ID:" + teacherId);
        System.out.println("Name:" + name);
        System.out.println("LastName:" + teacherLastName);
        System.out.println("Student LastName:" + studentLastName);
        System.out.println("CourseName:" + courseName);
    }

    public void printCourseDetails(int courseId, String name) {
        System.out.println("ID:" + courseId);
        System.out.println("Name:" + name);
    }
}
