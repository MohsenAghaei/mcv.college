

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBConnect {
    Student S;
    ArrayList<Student> students = new ArrayList();
    Teacher T;
    ArrayList<Teacher> teachers = new ArrayList();
    Course C;
    ArrayList<Course> courses = new ArrayList();

    public DBConnect() {
    }

    public void listOfStudent(Student S, ArrayList<Student> students) {
        try {
            String host = "jdbc:mysql://localhost:3306/college";
            String uName = "root";
            String uPass = "1630029300mA";
            Connection connection = DriverManager.getConnection(host, uName, uPass);
            Statement statement = connection.createStatement();
            String sql = "select * from student";
            ResultSet resultSet = statement.executeQuery(sql);

            while(resultSet.next()) {
                int student_id = resultSet.getInt("student_Id");
                String name = resultSet.getString("name");
                String last_name = resultSet.getString("last_name");
                int score = resultSet.getInt("score");
                int teacherId = resultSet.getInt("teacher_Id");
                int course_Id = resultSet.getInt("course_Id");
                S = new Student(student_id, name, last_name, score, teacherId, course_Id);
                students.add(S);
                System.out.println(S);
            }
        } catch (SQLException var16) {
            System.out.println(var16.getMessage());
        }

    }

    public void listOfTeacher(Teacher T, ArrayList<Teacher> teachers) {
        try {
            String host = "jdbc:mysql://localhost:3306/college";
            String uName = "root";
            String uPass = "1630029300mA";
            Connection connection = DriverManager.getConnection(host, uName, uPass);
            Statement statement = connection.createStatement();
            String sql = "select * from teacher";
            ResultSet resultSet = statement.executeQuery(sql);

            while(resultSet.next()) {
                int teacherId = resultSet.getInt("teacher_Id");
                String name = resultSet.getString("name");
                String last_name = resultSet.getString("last_name");
                int student_Id = resultSet.getInt("student_Id");
                int course_Id = resultSet.getInt("course_Id");
                T = new Teacher(teacherId, name, last_name, student_Id, course_Id);
                teachers.add(T);
                System.out.println(T);
            }
        } catch (SQLException var15) {
            System.out.println(var15.getMessage());
        }

    }

    public void listOfCourse(Course C, ArrayList<Course> courses) {
        try {
            String host = "jdbc:mysql://localhost:3306/college";
            String uName = "root";
            String uPass = "1630029300mA";
            Connection connection = DriverManager.getConnection(host, uName, uPass);
            Statement statement = connection.createStatement();
            String sql = "select * from course";
            ResultSet resultSet = statement.executeQuery(sql);

            while(resultSet.next()) {
                int course_Id = resultSet.getInt("course_Id");
                String name = resultSet.getString("name");
                int teacher_Id = resultSet.getInt("teacher_Id");
                int student_Id = resultSet.getInt("student_Id");
                C = new Course(course_Id, name, teacher_Id, student_Id);
                courses.add(C);
                System.out.println(C);
            }
        } catch (SQLException var14) {
            System.out.println(var14.getMessage());
        }

    }

    public void InsertStudentIntoList() {
        try {
            String host = "jdbc:mysql://localhost:3306/college";
            String uName = "root";
            String uPass = "1630029300mA";
            Connection connection = DriverManager.getConnection(host, uName, uPass);
            Statement statement = connection.createStatement();
            statement.executeUpdate("insert into student(student_id,name,last_name,score,teacher_id,course_id)values ('103','reza','rezaei','16','203','303')");
        } catch (SQLException var6) {
            System.out.println(var6.getMessage());
        }

    }

    public void InsertTeacherIntoList() {
        try {
            String host = "jdbc:mysql://localhost:3306/college";
            String uName = "root";
            String uPass = "1630029300mA";
            Connection connection = DriverManager.getConnection(host, uName, uPass);
            Statement statement = connection.createStatement();
            statement.executeUpdate("insert into teacher(teacher_id,name,last_name,student_id,course)values ('203','yousef','jallili','103','303')");
        } catch (SQLException var6) {
            System.out.println(var6.getMessage());
        }

    }

    public void InsertCourseIntoList() {
        try {
            String host = "jdbc:mysql://localhost:3306/college";
            String uName = "root";
            String uPass = "1630029300mA";
            Connection connection = DriverManager.getConnection(host, uName, uPass);
            Statement statement = connection.createStatement();
            statement.executeUpdate("insert into course(course_id,name,teacher_id,student_id)values ('303','java','203','103')");
        } catch (SQLException var6) {
            System.out.println(var6.getMessage());
        }

    }
}
