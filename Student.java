//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Stream;

public class Student extends Person {
    private static Student Students;
    private int studentId;
    private int score;
    private Teacher teacher;
    private Course course;

    public Student() {
        super();
    }

    protected Student(int studentId, String name, String lastName, int score, int teacherId, int courseId) {
        super(name, lastName);
        this.studentId = studentId;
        this.score = score;
    }

    public Student(int studentId, String name, String lastName, int score) {
        super(name, lastName);
        this.studentId = studentId;
        this.score = score;
    }

    public int getStudentId() {
        return this.studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getTeacherName() {
        return this.teacher.getLastName();
    }

    public void setTeacherName(String lastName) {
        this.teacher.setLastName(lastName);
    }

    public String getCourseName() {
        return this.course.getName();
    }

    public void setCourseName(String name) {
        this.course.setName(name);
    }

    public int getId() {
        return this.studentId;
    }

    public void setId(int studentId) {
        this.studentId = studentId;
    }

    public void addStudent(ArrayList<Student> students, ArrayList<Teacher> teachers, ArrayList<Course> courses, Scanner input) {
        System.out.println("Inter the student ID");
        int studentId = input.nextInt();
        System.out.println("Inter the name");
        String name = input.next();
        System.out.println("Inter the lastName");
        String lastName = input.next();
        System.out.println("Inter the score");
        int score = input.nextInt();
        System.out.println("Enter the Teacher Id");

        int teacherId;
        for(teacherId = 0; teacherId < teachers.size(); ++teacherId) {
            System.out.println(((Teacher)teachers.get(teacherId)).getId() + " " + ((Teacher)teachers.get(teacherId)).getLastName() + "\n");
        }

        teacherId = input.nextInt();
        System.out.println("Enter the Course Id");

        int courseId;
        for(courseId = 0; courseId < courses.size(); ++courseId) {
            System.out.println(((Course)courses.get(courseId)).getCourseId() + " " + ((Course)courses.get(courseId)).getName() + "\n");
        }

        courseId = input.nextInt();
        Student S = new Student(studentId, name, lastName, score, ((Teacher)teachers.get(teacherId)).getId(), ((Course)courses.get(courseId)).getCourseId());
        students.add(S);
        System.out.println("Successful");
    }

    public static void listOfStudent(ArrayList<Student> students, Teacher teacher, Course course) {
        for(int i = 0; i < students.size(); ++i) {
            Student S = new Student(((Student)students.get(i)).getStudentId(), ((Student)students.get(i)).getName(), ((Student)students.get(i)).getLastName(), ((Student)students.get(i)).getScore(), teacher.getTeacherId(), course.getCourseId());
            System.out.println("Code :" + i + " " + S);
        }

    }

    public static void listStudentPass(ArrayList<Student> students) {
        Stream<Student> filter = students.stream().filter((s) -> {
            return s.score > 12;
        });
        filter.forEach((student) -> {
            System.out.println(student.getName() + ": " + student.score);
        });
    }


}
