//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

public class Course {
    private String name;
    private int courseId;
    private Teacher teacher;
    private Student student;

    public Course() {
    }

    public Course(int courseId, String name, int teacherId, int studentId) {
        this.courseId = courseId;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCourseId() {
        return this.courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }


    public void setTeacherName(String lastName) {
        this.teacher.setLastName(lastName);
    }

    public String getStudentName() {
        return this.student.getLastName();
    }

    public void setStudentName(String name) {
        this.student.setLastName(name);
    }

    public String toString() {
        return "\t ID: " + this.courseId + "  Name: " + this.name ;
                //+ " Student Name:" + student.getLastName();
    }
}
